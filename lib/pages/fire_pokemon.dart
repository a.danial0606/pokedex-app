import 'package:flutter/material.dart';
import '../model/pokemon_model.dart';
import 'DetailPage.dart';

class FirePokemon extends StatefulWidget {
  @override
  FirePokemonState createState() {
    return new FirePokemonState();
  }
}

class FirePokemonState extends State<FirePokemon> {
   List firePoke;

  @override
  void initState() {
    firePoke = getFirePokemon();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(PokemonModel pokemon) => ListTile(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: new BoxDecoration(
                border: new Border(
                    right: new BorderSide(width: 1.0, color: Colors.white24))),
            child:  new CircleAvatar(
                  foregroundColor: Theme.of(context).primaryColor,
                  backgroundColor: Colors.grey,
                  backgroundImage: new NetworkImage(pokemon.avatarUrl),
                ),
          ),
          title: Text(
            pokemon.name,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),

          subtitle: Row(
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Container(
                    child: LinearProgressIndicator(
                        backgroundColor: Color.fromRGBO(209, 224, 224, 0.2),
                        value: pokemon.powerIndicatorValue,
                        valueColor: AlwaysStoppedAnimation(Colors.green)),
                  )),
              Expanded(
                flex: 4,
                child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(pokemon.type,
                        style: TextStyle(color: Colors.white))),
              )
            ],
          ),
          trailing:
              Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailPage(pokemon: pokemon)));
          },
        );

    Card makeCard(PokemonModel pokemon) => Card(
          elevation: 8.0,
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
          child: Container(
            decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
            child: makeListTile(pokemon),
          ),
        );

    final makeBody = Container(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: firePoke.length,
        itemBuilder: (BuildContext context, int index) {
          return makeCard(firePoke[index]);
        },
      ),
    );

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      title: Text("Fire Pokemon List"),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.list),
          onPressed: () {},
        )
      ],
    );

    return Scaffold(
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: topAppBar,
      body: makeBody,
    );
  }
}

List getFirePokemon() {
  return [
     PokemonModel(
      name: "Charmender",
      description: "It evolves into Charmeleon starting at level 16, which evolves into Charizard starting at level 36.",
      type: "Fire",
      powerIndicatorValue: 0.76,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Charmander_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/7/73/004Charmander.png"),
   PokemonModel(
      name: "Vulpix",
      powerIndicatorValue: 0.54,
      description: "It evolves into Ninetales when exposed to a Fire Stone.",
      type: "Fire",
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Vulpix_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/6/60/037Vulpix.png"),
   PokemonModel(
      name: "Cyndaquil",
      powerIndicatorValue: 0.44,
      description: "It evolves into Quilava starting at level 14, which evolves into Typhlosion starting at level 36.",
      type: "Fire",
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Cyndaquil_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/9/9b/155Cyndaquil.png"),
   PokemonModel(
      name: "Moltres",
      powerIndicatorValue: 0.67,
      description: "It is not known to evolve into or from any other Pokémon.",
      type: "Fire",
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Moltres_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/1/1b/146Moltres.png"),
   PokemonModel(
      name: "Charizard",
      powerIndicatorValue: 1.0,
      description: "It evolves from Charmeleon starting at level 36. It is the final form of Charmander. It can Mega Evolve into two forms: Mega Charizard X using Charizardite X and Mega Charizard Y using Charizardite Y.",
      type: "Fire",
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Charizard_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/7/7e/006Charizard.png"),
   PokemonModel(
      name: "Magmar",
      powerIndicatorValue: 0.54,
      description: "It evolves from Magby starting at level 30 and evolves into Magmortar when traded holding a Magmarizer.",
      type: "Fire",
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Magmar_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/8/8c/126Magmar.png"),
   PokemonModel(
      name: "Rapidash",
      powerIndicatorValue: 0.74,
      description: "It evolves from Ponyta starting at level 40.",
      type: "Fire",
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Rapidash_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/3/3f/078Rapidash.png")                                   
    ];
}