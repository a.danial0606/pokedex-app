import 'package:flutter/material.dart';
import '../model/pokemon_model.dart';
import 'DetailPage.dart';

class WaterPokemon extends StatefulWidget {
  @override
  WaterPokemonState createState() {
    return new WaterPokemonState();
  }
}

class WaterPokemonState extends State<WaterPokemon> {
    List waterPoke;

  @override
  void initState() {
    waterPoke = getWaterPokemon();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(PokemonModel pokemon) => ListTile(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: new BoxDecoration(
                border: new Border(
                    right: new BorderSide(width: 1.0, color: Colors.white24))),
            child:  new CircleAvatar(
                  foregroundColor: Theme.of(context).primaryColor,
                  backgroundColor: Colors.grey,
                  backgroundImage: new NetworkImage(pokemon.avatarUrl),
                ),
          ),
          title: Text(
            pokemon.name,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),

          subtitle: Row(
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Container(
                    child: LinearProgressIndicator(
                        backgroundColor: Color.fromRGBO(209, 224, 224, 0.2),
                        value: pokemon.powerIndicatorValue,
                        valueColor: AlwaysStoppedAnimation(Colors.green)),
                  )),
              Expanded(
                flex: 4,
                child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(pokemon.type,
                        style: TextStyle(color: Colors.white))),
              )
            ],
          ),
          trailing:
              Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailPage(pokemon: pokemon)));
          },
        );

    Card makeCard(PokemonModel pokemon) => Card(
          elevation: 8.0,
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
          child: Container(
            decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
            child: makeListTile(pokemon),
          ),
        );

    final makeBody = Container(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: waterPoke.length,
        itemBuilder: (BuildContext context, int index) {
          return makeCard(waterPoke[index]);
        },
      ),
    );

    final makeBottom = Container(
      height: 55.0,
      child: BottomAppBar(
        color: Color.fromRGBO(58, 66, 86, 1.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.home, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.blur_on, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.hotel, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.account_box, color: Colors.white),
              onPressed: () {},
            )
          ],
        ),
      ),
    );
    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      title: Text("Water Pokemon List"),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.list),
          onPressed: () {},
        )
      ],
    );

    return Scaffold(
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: topAppBar,
      body: makeBody,
      bottomNavigationBar: makeBottom,
    );
  }
}

List getWaterPokemon() {
  return [
     PokemonModel(
      name: "Squirtle",
      description: "It evolves into Wartortle starting at level 16, which evolves into Blastoise starting at level 36.",
      type: "Water",
      powerIndicatorValue: 0.4,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Squirtle_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/3/39/007Squirtle.png"),
     PokemonModel(
      name: "Golduck",
      description: "It evolves from Psyduck starting at level 33.",
      type: "Water",
      powerIndicatorValue: 0.6,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Golduck_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/f/fe/055Golduck.png"),
     PokemonModel(
      name: "Tentacruel",
      description: "It evolves from Tentacool starting at level 30.",
      type: "Water",
      powerIndicatorValue: 0.33,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Tentacruel_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/f/f6/073Tentacruel.png"),
    PokemonModel(
      name: "Gyarados ",
      description: "It evolves from Magikarp starting at level 20. It can Mega Evolve into Mega Gyarados using the Gyaradosite.",
      type: "Water",
      powerIndicatorValue: 0.9,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Gyarados_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/4/41/130Gyarados.png"),
    PokemonModel(
      name: "Poliwrath",
      description: "It evolves from Poliwhirl when exposed to a Water Stone. It is one of Poliwag's final forms, the other being Politoed.",
      type: "Water",
      powerIndicatorValue: 0.5,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Poliwrath_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/2/2d/062Poliwrath.png")
    ];
}