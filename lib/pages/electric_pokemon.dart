import 'package:flutter/material.dart';
import '../model/pokemon_model.dart';
import 'DetailPage.dart';

class ElectricPokemon extends StatefulWidget {
  @override
  ElectricPokemonState createState() {
    return new ElectricPokemonState();
  }
}

class ElectricPokemonState extends State<ElectricPokemon> {
  List electricPoke;

  @override
  void initState() {
    electricPoke = getElectricPokemmon();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(PokemonModel pokemon) => ListTile(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: new BoxDecoration(
                border: new Border(
                    right: new BorderSide(width: 1.0, color: Colors.white24))),
            child:  new CircleAvatar(
                  foregroundColor: Theme.of(context).primaryColor,
                  backgroundColor: Colors.grey,
                  backgroundImage: new NetworkImage(pokemon.avatarUrl),
                ),
          ),
          title: Text(
            pokemon.name,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),

          subtitle: Row(
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Container(
                    child: LinearProgressIndicator(
                        backgroundColor: Color.fromRGBO(209, 224, 224, 0.2),
                        value: pokemon.powerIndicatorValue,
                        valueColor: AlwaysStoppedAnimation(Colors.green)),
                  )),
              Expanded(
                flex: 4,
                child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(pokemon.type,
                        style: TextStyle(color: Colors.white))),
              )
            ],
          ),
          trailing:
              Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailPage(pokemon: pokemon)));
          },
        );

    Card makeCard(PokemonModel pokemon) => Card(
          elevation: 8.0,
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
          child: Container(
            decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
            child: makeListTile(pokemon),
          ),
        );

    final makeBody = Container(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: electricPoke.length,
        itemBuilder: (BuildContext context, int index) {
          return makeCard(electricPoke[index]);
        },
      ),
    );

    final makeBottom = Container(
      height: 55.0,
      child: BottomAppBar(
        color: Color.fromRGBO(58, 66, 86, 1.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.home, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.blur_on, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.hotel, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.account_box, color: Colors.white),
              onPressed: () {},
            )
          ],
        ),
      ),
    );
    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      title: Text("Electric Pokemon List"),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.list),
          onPressed: () {},
        )
      ],
    );

    return Scaffold(
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: topAppBar,
      body: makeBody,
      bottomNavigationBar: makeBottom,
    );
  }
}

List getElectricPokemmon() {
  return [
    PokemonModel(
      name: "Pikachu",
      description: "It evolves from Pichu when leveled up with high friendship and evolves into Raichu when exposed to a Thunder Stone. However, the starter Pikachu in Pokémon Yellow will refuse to evolve into Raichu unless it is traded and evolved on another save file.",
      type: "Electric",
      powerIndicatorValue: 0.44,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Pikachu_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/0/0d/025Pikachu.png"),
    PokemonModel(
      name: "Magnemite",
      description: "It evolves into Magneton starting at level 30, which evolves into Magnezone when leveled up in a special magnetic field.",
      type: "Electric",
      powerIndicatorValue: 0.2,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Magnemite_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/6/6c/081Magnemite.png"),
    PokemonModel(
      name: "Jolteon",
      description: "It evolves from Eevee when exposed to a Thunder Stone. It is one of Eevee's final forms, the others being Vaporeon, Flareon, Espeon, Umbreon, Leafeon, Glaceon, and Sylveon.",
      type: "Electric",
      powerIndicatorValue: 0.5,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Jolteon_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/b/bb/135Jolteon.png"),
    PokemonModel(
      name: "Zapdos",
      description: "It is not known to evolve into or from any other Pokémon.",
      type: "Electric",
      powerIndicatorValue: 0.8,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Zapdos_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/e/e3/145Zapdos.png"),

    PokemonModel(
      name: "Luxray",
      description: "It evolves from Luxio starting at level 30. It is the final form of Shinx.",
      type: "Electric",
      powerIndicatorValue: 1.0,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Luxray_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/a/a7/405Luxray.png"),

    PokemonModel(
      name: "Electivire",
      description: "It evolves from Electabuzz when traded holding an Electirizer. It is the final form of Elekid.",
      type: "Electric",
      powerIndicatorValue: 0.9,
      wikiUrl: "https://bulbapedia.bulbagarden.net/wiki/Electivire_(Pok%C3%A9mon)",
      avatarUrl:
          "https://cdn.bulbagarden.net/upload/2/23/466Electivire.png")               
    ];
}