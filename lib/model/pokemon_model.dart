class PokemonModel {
  final String name;
  final String description;
  final String type;
  final String avatarUrl;
  final String wikiUrl;
  final  double powerIndicatorValue;

  PokemonModel({this.name, this.powerIndicatorValue, this.wikiUrl, this.description, this.type, this.avatarUrl});


  // factory PokemonModel.fromJson(Map<String, dynamic> parsedJson) {
  //     var pokemonFromJson  = parsedJson['pokemon'];
  //     //print(streetsFromJson.runtimeType);
  //     // List<String> streetsList = new List<String>.from(streetsFromJson);
  //     String pokemonData = pokemonFromJson.cast<String>();

  //     return new PokemonModel(
  //       name: parsedJson['pokemon'],
  //       description: pokemonData,
  //       power: pokemonData,
  //       avatarUrl: pokemonData, 
  //     );
  //   }

}
