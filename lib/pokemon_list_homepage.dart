import 'package:flutter/material.dart';
import 'package:myapp/pages/electric_pokemon.dart';
import 'package:myapp/pages/water_pokemon.dart';
import 'package:myapp/pages/fire_pokemon.dart';

class PokemonHome extends StatefulWidget {
  @override
  _PokemonHomeState createState() => new _PokemonHomeState();
}

class _PokemonHomeState extends State<PokemonHome>
    with SingleTickerProviderStateMixin {
 
  @override
  void initState() {
    super.initState();
   
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => new ElectricPokemon()),
                  );
              },
              textColor: Colors.white,
              child: new Container(
                 child: new Text(
                   'Electric Type Pokemon',
                    textAlign: TextAlign.center,
                    style: new TextStyle(fontSize: 16.0, color: Colors.white),
                    ),
                 width: 200.0,
                 height: 30.0,
              ),
              color: Colors.blue,
            ),
            RaisedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => new WaterPokemon()),
                  );
              },
              textColor: Colors.white,
              child: new Container(
                 child: new Text('Water Type Pokemon',
                    textAlign: TextAlign.center,
                    style: new TextStyle(fontSize: 16.0, color: Colors.white),
                    ),
                 width: 200.0,
                 height: 30.0,
              ),
              color: Colors.blue,
            ),
             RaisedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => new FirePokemon()),
                  );
              },
              textColor: Colors.white,
              child: new Container(
                 child: Text('Fire Type Pokemon',
                    textAlign: TextAlign.center,
                    style: new TextStyle(fontSize: 16.0, color: Colors.white),
                    ),
                 width: 200.0,
                 height: 30.0,
              ),
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
}
